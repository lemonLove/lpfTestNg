package com.canyou.system;

import com.canyou.utils.ElementAction;
import com.canyou.utils.Locator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.List;

/**
 * @author lpf
 * @create 2017-11-09 16:47
 **/
public class AppliancePage {

    private Locator addBtn = new Locator("btnAdd", Locator.ByType.id);

    private Locator applianceName = new Locator("txtApplianceName", Locator.ByType.id);

    private Locator ip = new Locator("applianceIp", Locator.ByType.id);

    private Locator port = new Locator("//input[@name='appliance.PORT']", Locator.ByType.xpath);

    private Locator timeout = new Locator("//input[@name='appliance.TIMEOUT']", Locator.ByType.xpath);

    private Locator userName = new Locator("//input[@name='appliance.APPLIANCEUSERNAME']", Locator.ByType.xpath);

    private Locator password = new Locator("//input[@name='appliance.APPLIANCEPASSWORD']", Locator.ByType.xpath);

    private Locator statusYes = new Locator("//input[@value='0']", Locator.ByType.xpath);

    private Locator statusNo = new Locator("//input[@value='1']", Locator.ByType.xpath);

    private Locator rsaupload = new Locator("rsaupload", Locator.ByType.id);

    private Locator file = new Locator("//input[@name='file']", Locator.ByType.xpath);

    private Locator remarks = new Locator("//textarea[@name='appliance.REMARK']", Locator.ByType.xpath);

    private Locator testAppliance = new Locator("testRunStatus", Locator.ByType.id);

    private Locator saveAppliance = new Locator("//button[@data-id='确定']", Locator.ByType.xpath);

    private Locator txtApplianceName = new Locator("applianceName", Locator.ByType.id);

    private Locator btnQuery = new Locator("query", Locator.ByType.id);

    private Locator okBtn = new Locator("//button[@data-id='ok']", Locator.ByType.xpath);


    public void addAppliance(String applianceNameStr, String ipStr,
                             String portStr, String timeouts,
                             String authName, String authPassword,
                             String uploadfile, String remarksStr)
            throws Exception {
        ElementAction action = new ElementAction();
        action.click(addBtn);

        action.sendKey(applianceName,applianceNameStr);
        action.sendKey(ip,ipStr);
        action.sendKey(port,portStr);
        action.clear(timeout);
        action.sendKey(timeout,timeouts);
        action.sendKey(userName,authName);
        action.sendKey(password,authPassword);
        action.click(statusYes);
        action.sendKey(rsaupload,ipStr);
        action.sendKey(file,uploadfile);
        action.sendKey(remarks,remarksStr);
        action.click(saveAppliance);
        //等待添加成功
        action.loadingFinished(new Locator("loading", Locator.ByType.id));
    }


    public void editAppliance(String applianceIp) throws InterruptedException {
        ElementAction action = new ElementAction();
        action.click(new Locator(applianceIp+"_editappliance",Locator.ByType.id));
        Thread.sleep(1000);
        action.click(saveAppliance);
        //等待编辑成功
        action.loadingFinished(new Locator("loading", Locator.ByType.id));

    }

    public void deleteAppliance(String applianceIp) throws InterruptedException {
        ElementAction action = new ElementAction();
        action.click(new Locator(applianceIp+"_deleteappliance",Locator.ByType.id));
        Thread.sleep(1000);
        action.click(okBtn);
    }


    public void statusSetting(String applianceIp) throws InterruptedException {
        ElementAction action = new ElementAction();
        action.click(new Locator(applianceIp+"_statusappliance",Locator.ByType.id));
        Thread.sleep(1000);
        action.click(okBtn);

    }

    public void testAppliance(){
        ElementAction action = new ElementAction();
        action.click(testAppliance);
    }

    public void searchAppliance(String name) {
        ElementAction action = new ElementAction();
        action.sendKey(txtApplianceName,name);
        action.click(btnQuery);
    }
}
