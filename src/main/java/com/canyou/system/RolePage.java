package com.canyou.system;

import com.canyou.utils.ElementAction;
import com.canyou.utils.Locator;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * @author lpf
 * @create 2017-11-09 16:47
 **/
public class RolePage{

    private Locator btnAdd = new Locator("btnAdd", Locator.ByType.id);

    private Locator btnQuery = new Locator("query", Locator.ByType.id);

    private Locator btnClear = new Locator("clearQuery", Locator.ByType.id);

    private Locator roleName = new Locator("//input[@name='role.ROLENAME']", Locator.ByType.xpath);

    private Locator txtRoleNameSearch = new Locator("roleName", Locator.ByType.id);

    private Locator saveRole = new Locator("//button[@data-id='确定']", Locator.ByType.xpath);

    private Locator btnOk = new Locator("//button[@data-id='ok']", Locator.ByType.xpath);



    public void addRole(String roleNameStr) throws InterruptedException {
        ElementAction action = new ElementAction();
        action.click(btnAdd);
        action.sendKey(roleName,roleNameStr);
        action.click(saveRole);
    }

    public void searchRole(String roleNameStr) throws InterruptedException {
        ElementAction action = new ElementAction();
        action.sendKey(txtRoleNameSearch,roleNameStr);
        action.click(btnQuery);
    }

    public void editRole(String roleNameStr) throws InterruptedException {
        ElementAction action = new ElementAction();
        action.click(new Locator(roleNameStr+"_editrole",Locator.ByType.id));
        Thread.sleep(1000);
        action.click(saveRole);
    }

    public void delete(String roleNameStr)throws InterruptedException{
        ElementAction action = new ElementAction();
        action.click(new Locator(roleNameStr+"_deleterole",Locator.ByType.id));
        Thread.sleep(1000);
        action.click(btnOk);
    }


    public void clear() {
        ElementAction action = new ElementAction();
        action.click(btnClear);
    }

    public void assignPermission(String roleNameStr) throws InterruptedException{
        ElementAction action = new ElementAction();
        action.click(new Locator(roleNameStr+"_permissionsetrole",Locator.ByType.id));
        Thread.sleep(1000);
        List<WebElement> allChecked = action.findElements(new Locator("p",Locator.ByType.className));
        for (int i = 0; i <allChecked.size(); i++) {
            allChecked.get(i).click();
            Thread.sleep(500);
        }
        action.click(saveRole);
    }

}
