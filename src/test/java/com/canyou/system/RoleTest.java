package com.canyou.system;

import com.canyou.DashBoardPage;
import com.canyou.system.RolePage;
import com.canyou.utils.ExcelReadUtil;
import com.canyou.utils.TestBaseCase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.*;


/**
 * @author lpf
 * @create 2017-11-13 09:40
 **/
public class RoleTest {

    private RolePage rolePage = null;
    private WebDriver driver;
    private DashBoardPage dashBoardPage;

    @DataProvider(name = "roleData")
    public Object[][] roleData() {
        //读取测试数据
        String filePath = "src/test/resources/data/roleData.xls";
        return ExcelReadUtil.case_data_excel(0, 1, 1, 0, 0, filePath);
    }

    @Test(description = "添加角色测试", dataProvider = "roleData")
    public void test001Add(String roleName) throws InterruptedException {
        rolePage.addRole(roleName);
        Thread.sleep(3000);
    }

    @Test(description = "搜索角色测试",dataProvider = "roleData")
    public void test002Search(String roleName) throws InterruptedException {
        rolePage.searchRole(roleName);
        Thread.sleep(3000);
    }

    @Test(description = "清空条件测试")
    public void test003Clear() throws InterruptedException {
        rolePage.clear();
        Thread.sleep(3000);
    }

    @Test(description = "编辑角色测试",dataProvider = "roleData")
    public void test004Edit(String roleName) throws InterruptedException {
        rolePage.editRole(roleName);
        Thread.sleep(3000);
    }

    @Test(description = "角色分配权限测试",dataProvider = "roleData")
    public void test005AssignPermission(String roleName) throws InterruptedException {
        rolePage.assignPermission(roleName);
        Thread.sleep(3000);
    }
    @Test(description = "删除角色测试",dataProvider = "roleData")
    public void test006Delete(String roleNameStr) throws InterruptedException {
        rolePage.delete(roleNameStr);
        Thread.sleep(3000);
    }

    @BeforeClass
    public void BeforeClass() throws InterruptedException {
        driver = TestBaseCase.driver;
        dashBoardPage = TestBaseCase.dashBoardPage;
        dashBoardPage.navigateToRole();
        rolePage = PageFactory.initElements(driver, RolePage.class);
    }


    //登录切换写在这里
    @BeforeMethod
    public void beforMethod() throws InterruptedException {

    }

    @AfterMethod
    public void afterMethod() throws InterruptedException {
        //浏览器关闭可以写在这里

    }

    @AfterClass
    public void afterClass() throws InterruptedException {

    }


}
