package com.canyou.system;

import com.canyou.utils.ElementAction;
import com.canyou.utils.Locator;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * @author lpf
 * @create 2017-11-09 16:47
 **/
public class SmtpPage {

    private Locator smtpConfig = new Locator("//a[@href='#emailNotify']", Locator.ByType.xpath);

    private Locator btnSubmit = new Locator("btnSaveinfo", Locator.ByType.id);

    private Locator btnTest = new Locator("btnTest", Locator.ByType.id);

    private Locator btnConfig = new Locator("btnSaveConfig", Locator.ByType.id);

    private Locator smtpserverName = new Locator("smtpservername", Locator.ByType.id);

    private Locator port = new Locator("//input[@name='smtpServer.SERVERPORT']", Locator.ByType.xpath);

    private Locator fromemailDress = new Locator("//input[@name='smtpServer.FROMEMAILADDRESS']", Locator.ByType.xpath);

    private Locator email = new Locator("//input[@name='smtpServer.EMAIL']", Locator.ByType.xpath);

    private Locator password = new Locator("//input[@type='password']", Locator.ByType.xpath);

    private Locator messageLimit = new Locator("//input[@name='smtpServer.MESSAGELIMIT']", Locator.ByType.xpath);

    private Locator useSSL = new Locator("//input[@name='smtpServer.USESSL']", Locator.ByType.xpath);

    private Locator testEmail = new Locator("//input[@name='testEmail']", Locator.ByType.xpath);

    private Locator testEmailBtn = new Locator("//button[@data-id='确定']", Locator.ByType.xpath);

    private Locator testEmailCancer = new Locator("//button[@data-id='取消']", Locator.ByType.xpath);

    private Locator timeInterval = new Locator("//input[@name='smtpServer.INTERVAL']", Locator.ByType.xpath);

    private Locator emailObject = new Locator("//textarea[@name='smtpServer.EMAILRECEPIENTS']", Locator.ByType.xpath);

    ElementAction action = new ElementAction();

    public void submit(String emailService,String portStr,boolean isSsl,
                       String sendEmailAddress,String emailStr,String passwordStr,
                       String limit) throws Exception {
        action.clear(smtpserverName);
        action.click(smtpserverName);
        action.sendKey(smtpserverName,emailService);
        action.clear(port);
        action.sendKey(port,portStr);
        if(isSsl){
            WebElement webElement = action.findElement(useSSL);
            if(!webElement.isSelected()){
                action.click(useSSL);
            }
        }
        action.clear(fromemailDress);
        action.sendKey(fromemailDress,sendEmailAddress);
        action.clear(email);
        action.sendKey(email,emailStr);
        action.clear(password);
        action.sendKey(password,passwordStr);
        action.clear(messageLimit);
        action.sendKey(messageLimit,limit);
        Thread.sleep(1000);
        action.click(btnSubmit);
    }

    public void testSmtp(String email) throws InterruptedException {
        action.click(btnTest);
        Thread.sleep(1000);
        action.sendKey(testEmail,email);
        action.click(testEmailBtn);
        Thread.sleep(3000);
        action.click(testEmailCancer);
    }

    public void configSetting(boolean status,String filters,String interval,String emainl) throws Exception {
        action.click(smtpConfig);
        Thread.sleep(1000);
        List<WebElement> smtpStatus = action.findElements(new Locator("//input[@type='radio']", Locator.ByType.xpath));
        List<WebElement> filterElement = action.findElements(new Locator("//input[@name='smtpServer.FILTER']", Locator.ByType.xpath));
        if(status){
            smtpStatus.get(0).click();
        }else{
            smtpStatus.get(1).click();
        }
        if(filters.contains("warning")){
            WebElement w = filterElement.get(0);
            if(!w.isSelected()){
                w.click();
            }
        }else{
            WebElement w = filterElement.get(0);
            if(w.isSelected()){
                w.click();
            }
        }
        if(filters.contains("error")){
            WebElement w = filterElement.get(1);
            if(!w.isSelected()){
                w.click();
            }
        }else{
            WebElement w = filterElement.get(1);
            if(w.isSelected()){
                w.click();
            }
        }
        action.clear(timeInterval);
        action.sendKey(timeInterval,interval);
        action.clear(emailObject);
        action.sendKey(emailObject,emainl);
        action.click(btnConfig);

    }
}
