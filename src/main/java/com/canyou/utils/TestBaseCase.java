package com.canyou.utils;


import com.canyou.DashBoardPage;
import com.canyou.LoginPage;
import com.canyou.selenium.DriverEngine;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import java.net.MalformedURLException;
import java.util.Map;


/**
 * @author lpf
 * @create 2017-11-20 17:36
 **/
public class TestBaseCase {
    public static WebDriver driver;

    public static DashBoardPage dashBoardPage;

    public LoginPage loginPage;

    @BeforeSuite
    public void testBeforeSuite() throws InterruptedException {
        DriverEngine driverEngine = new DriverEngine();
        driver = driverEngine.getChromeDriver();
        driver.manage().window().maximize();
        //登录
        Map<String,String> paramMap = ResourceKit.readProperties("conf/config.properties");
        driver.get(paramMap.get("loginUrl"));
        dashBoardPage = PageFactory.initElements(driver, DashBoardPage.class);
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        Assert.assertNotNull(loginPage);
        loginPage.login("Manager", "000000");
    }

    @BeforeTest
    public void beforeTest() throws MalformedURLException, InterruptedException {

    }

    @AfterTest
    public void afterTest() {
    }

    @AfterSuite
    public void testAfterSuite() {
        if (driver != null) {
            driver.close();
        }
    }

}
