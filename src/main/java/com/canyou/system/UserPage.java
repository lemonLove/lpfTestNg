package com.canyou.system;

import com.canyou.utils.ElementAction;
import com.canyou.utils.Locator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

/**
 * @author lpf
 * @create 2017-11-09 16:47
 **/
public class UserPage {

    private Locator btnAdd = new Locator("btnAdd", Locator.ByType.id);

    private Locator userName = new Locator("//input[@name='user.USERNAME']", Locator.ByType.xpath);

    private Locator password = new Locator("//input[@name='user.PASSWORD']", Locator.ByType.xpath);

    private Locator checkPassword = new Locator("//input[@recheck='user.PASSWORD']", Locator.ByType.xpath);

    private Locator trueName = new Locator("//input[@name='user.REALNAME']", Locator.ByType.xpath);

    private Locator email = new Locator("//input[@name='user.EMAIL']", Locator.ByType.xpath);

    private Locator isSensitive = new Locator("//input[@id='ckSensitive']", Locator.ByType.xpath);

    private Locator remarks = new Locator("//textarea[@name='user.REMARK']", Locator.ByType.xpath);

    private Locator saveUser = new Locator("//button[@data-id='确定']", Locator.ByType.xpath);

    private Locator txtUserNameSearch = new Locator("txtUserName", Locator.ByType.id);

    private Locator btnQuery = new Locator("query", Locator.ByType.id);

    private Locator okBtn = new Locator("//button[@data-id='ok']", Locator.ByType.xpath);

    ElementAction action = new ElementAction();
    public void addUser(String userNameStr,String pwdStr,String checkPwd,String truthName,
                        String emailStr,boolean isSensi,String remarkStr) throws InterruptedException {
        action.click(btnAdd);
        action.sendKey(userName,userNameStr);
        action.sendKey(password,pwdStr);
        action.sendKey(checkPassword,checkPwd);
        action.sendKey(trueName,truthName);
        action.sendKey(email,emailStr);
        if(isSensi){
            action.click(isSensitive);
        }
        action.sendKey(remarks,remarkStr);
        action.click(saveUser);
    }



    public void searchUser(String searchName) {
        action.sendKey(txtUserNameSearch,searchName);
        action.click(btnQuery);
    }

    public void editUser(String userNameStr) throws InterruptedException {
        action.click(new Locator(userNameStr+"_edituser", Locator.ByType.id));
        Thread.sleep(1000);
        action.click(saveUser);
    }

    public void applianceAllot(String userNameStr,String applianceName,String applianceIp) throws InterruptedException {
        action.click(new Locator(userNameStr+"_applianceuser", Locator.ByType.id));
        Thread.sleep(1000);
        action.selectByText(new Locator("sel_all_area", Locator.ByType.id),applianceName+"("+applianceIp+")");
        action.click(new Locator("btn_choose_selected_area", Locator.ByType.id));
        Thread.sleep(1000);
        action.click(saveUser);
    }

    public void orgAllot(String userNameStr) throws InterruptedException {
        action.click(new Locator(userNameStr+"_orguser", Locator.ByType.id));
        Thread.sleep(1000);

        List<WebElement> allChecked = action.findElements(new Locator("orgIds", Locator.ByType.name));
        for (int i = 0; i <allChecked.size(); i++) {
            allChecked.get(i).click();
            Thread.sleep(300);
        }
        action.click(saveUser);
    }

    public void roleAllot(String userNameStr,String roleName) throws InterruptedException {

        action.click(new Locator(userNameStr+"_roleuser", Locator.ByType.id));
        Thread.sleep(1000);
        WebElement webElement = action.findElement(new Locator(roleName, Locator.ByType.id));
        if(!webElement.isSelected()){
            webElement.click();
        }
        action.click(saveUser);
    }

    public void deleteUser(String userNameStr) throws InterruptedException {
        action.click(new Locator(userNameStr+"_deleteuser", Locator.ByType.id));
        Thread.sleep(1000);
        action.click(okBtn);
    }


}
