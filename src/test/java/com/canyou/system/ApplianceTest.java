package com.canyou.system;

import com.canyou.DashBoardPage;
import com.canyou.utils.ExcelReadUtil;
import com.canyou.utils.TestBaseCase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.*;


/**
 * @author lpf
 * @create 2017-11-13 09:40
 **/
public class ApplianceTest {

    private WebDriver driver = null;
    private DashBoardPage dashBoardPage = null;
    private AppliancePage appliancePage = null ;

    @DataProvider(name = "applianceData")
    public Object[][] applianceData() {
        //读取测试数据
        String filePath = "src/test/resources/data/applianceData.xls";
        return ExcelReadUtil.case_data_excel(0, 1, 1, 0, 7, filePath);
    }

    @DataProvider(name = "applianceIpData")
    public Object[][] applianceIpData() {
        //读取测试数据
        String filePath = "src/test/resources/data/applianceData.xls";
        return ExcelReadUtil.case_data_excel(0, 1, 1, 1, 1, filePath);
    }

    @DataProvider(name = "applianceNameData")
    public Object[][] applianceNameData() {
        //读取测试数据
        String filePath = "src/test/resources/data/applianceData.xls";
        return ExcelReadUtil.case_data_excel(0, 1, 1, 0, 0, filePath);
    }

    @Test(dataProvider = "applianceData",description = "添加节点测试")
    public void test001Add(String applianceName,String applianceIp,String port,
                           String timeout,String authName,String authPassword ,
                           String uploadFile,String remarks) throws Exception {
        appliancePage.addAppliance(applianceName,applianceIp,port,
                timeout,authName,authPassword,uploadFile,remarks);
        Thread.sleep(1000);
    }

    @Test(description = "搜索节点测试",dataProvider = "applianceNameData")
    public void test002Search(String applianceNameStr) throws InterruptedException {
        appliancePage.searchAppliance(applianceNameStr);
        Thread.sleep(3000);
    }

    @Test(description = "编辑节点测试",dataProvider = "applianceIpData")
    public void test003Edit(String applianceIpStr) throws InterruptedException {
        appliancePage.editAppliance(applianceIpStr);
        Thread.sleep(1000);
    }

    @Test(description = "设置节点状态测试",dataProvider = "applianceIpData",invocationCount=2)
    public void test003SettingStatus(String applianceIpStr) throws InterruptedException {
        appliancePage.statusSetting(applianceIpStr);
        Thread.sleep(3000);

    }

    @Test(description = "删除节点测试",dataProvider = "applianceIpData")
    public void test004Delete(String applianceIpStr) throws InterruptedException {
        appliancePage.deleteAppliance(applianceIpStr);
        Thread.sleep(3000);
    }



    @BeforeClass
    public void BeforeClass() throws InterruptedException {
        driver = TestBaseCase.driver;
        dashBoardPage = TestBaseCase.dashBoardPage;
        dashBoardPage.navigateToAppliance();
        appliancePage = PageFactory.initElements(driver, AppliancePage.class);
        Thread.sleep(1000);
    }


    //登录切换写在这里
    @BeforeMethod
    public void beforMethod() throws InterruptedException {

    }

    @AfterMethod
    public void afterMethod() throws InterruptedException {
        //浏览器关闭可以写在这里

    }

    @AfterClass
    public void afterClass() throws InterruptedException {


    }


}
