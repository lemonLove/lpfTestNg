package com.canyou.system;

import com.canyou.LoginPage;
import com.canyou.utils.TestBaseCase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.*;


/**
 * @author lpf
 * @create 2017-11-13 09:40
 **/
public class LoginTest {
    private WebDriver driver;
    private LoginPage loginPage = null;


    @Test(description = "退出登录测试")
    public void test001LoginOut() throws InterruptedException {
        loginPage.loginOut();
        Thread.sleep(1000);
    }

    @Parameters({"userName","password"})
    @Test(description = "用户登录测试")
    public void test002Login(String username,String password) throws InterruptedException {
        loginPage.login(username,password);
        Thread.sleep(3000);
    }

    @BeforeClass
    public void BeforeClass() throws InterruptedException {
        driver = TestBaseCase.driver;
        Thread.sleep(1000);
        loginPage = PageFactory.initElements(driver, LoginPage.class);
    }


    @BeforeMethod
    public void beforMethod() throws InterruptedException {

    }

    @AfterMethod
    public void afterMethod() throws InterruptedException {
        //浏览器关闭可以写在这里

    }

    @AfterClass
    public void afterClass() throws InterruptedException {

    }


}
