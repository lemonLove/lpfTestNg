package com.canyou.system;

import com.canyou.DashBoardPage;
import com.canyou.system.UserPage;
import com.canyou.utils.ExcelReadUtil;
import com.canyou.utils.TestBaseCase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.annotations.*;


/**
 * @author lpf
 * @create 2017-11-13 09:40
 **/
public class UserTest {
    private WebDriver driver;
    private DashBoardPage dashBoardPage;
    private UserPage userPage = null ;

    @DataProvider(name = "applianceData")
    public Object[][] applianceData() {
        //读取测试数据
        String filePath = "src/test/resources/data/applianceData.xls";
        return ExcelReadUtil.case_data_excel(0, 1, 1, 0, 1, filePath);
    }
    @DataProvider(name = "roleData")
    public Object[][] roleData() {
        //读取测试数据
        String filePath = "src/test/resources/data/roleData.xls";
        return ExcelReadUtil.case_data_excel(0, 1, 1, 0, 0, filePath);
    }

    @Test(description = "添加用户测试")
    @Parameters({"userName","password"})
    public void test001Add(String userName,String password) throws InterruptedException {
        userPage.addUser(userName,password,"000000","lpf",
                "123@qq.com",true,"lpf");
        Thread.sleep(3000);
    }

    @Test(description = "用户编辑测试")
    @Parameters("userName")
    public void test002Edit(String userName) throws InterruptedException {
        userPage.editUser(userName);
        Thread.sleep(3000);
    }


    @Test(dataProvider = "applianceData",description = "用户分配节点测试")
    public void test003ApplianceAllot(ITestContext context,String applianceName, String applianceIp) throws InterruptedException {
        String userNameStr =  context.getCurrentXmlTest().getParameter("userName");
        userPage.applianceAllot(userNameStr,applianceName,applianceIp);
        Thread.sleep(3000);
    }

    @Test(description = "用户分配组织测试")
    @Parameters("userName")
    public void test004OrgAllot(String userName) throws InterruptedException {
        userPage.orgAllot(userName);
        Thread.sleep(3000);
    }

    @Test(dataProvider = "roleData",description = "用户分配角色测试")
    public void test005RoleAllot(ITestContext context,String roleName) throws InterruptedException {
        String userNameStr =  context.getCurrentXmlTest().getParameter("userName");
        userPage.roleAllot(userNameStr,roleName);
        Thread.sleep(3000);
    }

    @Parameters("userName")
    @Test(description = "删除用户测试")
    public void test006Delete(String userName) throws InterruptedException {
        userPage.deleteUser(userName);
        Thread.sleep(3000);
    }

    @BeforeClass
    public void BeforeClass() throws InterruptedException {
        driver = TestBaseCase.driver;
        dashBoardPage = TestBaseCase.dashBoardPage;
        dashBoardPage.navigateToUser();
        Thread.sleep(3000);
        userPage = PageFactory.initElements(driver, UserPage.class);
    }




    @BeforeMethod
    public void beforMethod() throws InterruptedException {

    }

    @AfterMethod
    public void afterMethod() throws InterruptedException {
        //浏览器关闭可以写在这里

    }

    @AfterClass
    public void afterClass() throws InterruptedException {

    }


}
