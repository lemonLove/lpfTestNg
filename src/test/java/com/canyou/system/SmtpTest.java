package com.canyou.system;

import com.canyou.DashBoardPage;
import com.canyou.LoginPage;
import com.canyou.selenium.DriverEngine;
import com.canyou.system.SmtpPage;
import com.canyou.utils.TestBaseCase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.*;


/**
 * @author lpf
 * @create 2017-11-13 09:40
 **/
public class SmtpTest {

    private WebDriver driver = null;
    private DashBoardPage dashBoardPage = null;
    private SmtpPage smtpPage = null ;

    @Test(description = "编辑邮件测试")
    public void test001Submit() throws Exception {

        smtpPage.submit("smtp.163.com","25",false,"yuzifen0719@163.com","yuzifen0719@163.com","","10");
        Thread.sleep(3000);
    }

    @Test(description = "邮件测试")
    public void test002Test() throws InterruptedException {
        smtpPage.testSmtp("894377318@qq.com");
        Thread.sleep(3000);
    }

    @Test(description = "邮件配置测试")
    public void test003Config() throws Exception {
        smtpPage.configSetting(true,"warning error","30","894377318@qq.com,3119215315@qq.com");
        Thread.sleep(3000);
    }

    @BeforeClass
    public void BeforeClass() throws InterruptedException {
        driver = TestBaseCase.driver;
        dashBoardPage = TestBaseCase.dashBoardPage;
        dashBoardPage.navigateToSmtp();
        smtpPage = PageFactory.initElements(driver, SmtpPage.class);
    }


    //登录切换写在这里
    @BeforeMethod
    public void beforMethod() throws InterruptedException {

    }

    @AfterMethod
    public void afterMethod() throws InterruptedException {
        //浏览器关闭可以写在这里

    }

    @AfterClass
    public void afterClass() throws InterruptedException {

    }


}
